Comment Luv Drupal is the Drupal based version of the popular Wordpress plugin.

When someone posts a comment on your site Comment Luv attempts to find their latest blog post and add the title and link to the bottom of their comment. Comment Luv is great for attracting people to comment on your blogs and a great way to build back links to your site.

Installation

1. Upload the commentluv directory to your modules folder.

2. Download Magpie RSS from Sourceforge and copy all the files into the commentluv directory.  http://sourceforge.net/project/showfiles.php?group_id=55691
3. Go to the modules section in the administration section and enable.

Limitations

There are a free limitations currently that I hope to work on over the coming releases.

1. Once enabled the comment luv plugin works on all content types, there is no way to turn it off for certain content types (if there is demand for this feature I will write it).

2. Currently Comment Luv only works for anonymous users, I intend to add in functionality for authenticated users soon.

3. Not tested on Drupal 6. 